#ifndef SIMPSH_H_INCLUDED
#define SIMPSH_H_INCLUDED

#include <stdbool.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/time.h>

#define NUM_LONG_OPTS 25


///////////////////////////////////////////////////////////////
// GLOBAL STRUCTS
///////////////////////////////////////////////////////////////
  
const struct option LONG_OPTS[NUM_LONG_OPTS + 1] = {
  /* 63 == '?'. Don't use 63 as val. */
  /* 58 == ':'. Don't use 58 as val. */
  
  { "append", no_argument, 0, 0 },
  { "cloexec", no_argument, 0, 1 },
  { "creat", no_argument, 0, 2 },
  { "directory", no_argument, 0, 3 },
  { "dsync", no_argument, 0, 4 },
  { "excl", no_argument, 0, 5 },
  { "nofollow", no_argument, 0, 6 },
  { "nonblock", no_argument, 0, 7 },
  { "rsync", no_argument, 0, 8 },
  { "sync", no_argument, 0, 9 },
  { "trunc", no_argument, 0, 10 },
  { "rdonly", required_argument, 0, 11 },
  { "rdwr", required_argument, 0, 12 },
  { "wronly", required_argument, 0, 13 },
  { "pipe", no_argument, 0, 14 },
  { "command", required_argument, 0, 15 },
  { "wait", no_argument, 0, 16 },
  { "close", required_argument, 0, 17 },
  { "verbose", no_argument, 0, 18 },
  { "profile", no_argument, 0, 19 },
  { "abort", no_argument, 0, 20 },
  { "catch", required_argument, 0, 21 },
  { "ignore", required_argument, 0, 22 },
  { "default", required_argument, 0, 23 },
  { "pause", no_argument, 0, 24 },
  { 0, 0, 0, 0 }
};

typedef struct {
  pid_t cpid;
  // cpid is the child pid
  char** cmd_array;
  // cmd_array stores the arguments to --command option,
  // excluding in, out, err. For example, --command 2 4 6 cat b -
  // stores { "cat", "b", "-" }    
} ChildToCommand;

typedef struct {
  int read_fd;
  int write_fd;
} Pipe;

///////////////////////////////////////////////////////////////
// GLOBAL VARIABLES
///////////////////////////////////////////////////////////////

int status;
// temporary variable to store exit status.

int n_fds;
// stores the number of file descriptors created.

int fd_ind = 0;
// fd_ind is the logical file descriptor.
// fd_ind is the index to the array of actual file descriptors, fd[].
// The first three arguments to --command option indicate logical
// file descriptors.

int* fd;
// The array of actual file descriptors. The index is the "logical"
// file descriptor.

int cpids_to_cmds_ind = 0;
// index to cpids_to_cmds[], which maps cpids to --command option
// arguments, excluding the first three arguments.

char** cmd_array = NULL;
// contains the command for a particular process (using execvp)

int cmd_array_size;
// the size of the argument to execvp, which is the number of arguments
// to --command option excluding the first three

int pipe_fd[2];
int shell_exit_status = 0;
// is the maximum of all child processes' exit statuses.

ChildToCommand* cpids_to_cmds = NULL;
// array of ChildToCommand structs

int nCmds;
// count the number of commands

Pipe* pipes = NULL;
// array of Pipes

int nPipes;
// number of pipes

int pipes_ind = 0;
// index to pipes[]

bool profile_on = false;

  
///////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
///////////////////////////////////////////////////////////////
/*
void fill_cmd_array(char** cmd_array, char** argv);
// cmd_array stores the arguments to --command option,
// excluding in, out, err
  
void redirect(int in, int out, int err);
// in, out, and err are actual fds that serve as
// input, output, and stderr to --command option.

int count_fds(int argc, char** argv);
// counts how many file descriptors the program needs.

int count_cmd_array_strings(int argc, char** argv) 
// counts the number of arguments to a --command option, excluding
// in, out, err. For example, --command 0 1 2 sort -u
// causes count_cmd_array_strings to return 2.

  bool isDigit(char c);
// checks whether char c is a digit.

int count_commands(int argc, char** argv);
// counts how many --command options there are.

void insert_to_commands(pid_t cpid, char** cmd_array);
// Insert cpid and cmd_array to the next slot of cpids_to_cmds[]

int count_pipes(int argc, char** argv);
// count the number of pipes to create
*/

//////////////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS
//////////////////////////////////////////////////////////////////////
  
void redirect(int in, int out, int err) {
  // arguments are real file descriptors.
  dup2(in, 0);
  dup2(out, 1);
  dup2(err, 2);

  //  printf("return from redirect.\n");
}

void close_all_fds(void) {
  for (int i = 0; i < n_fds; i++) {
    close(fd[i]);
  }
}

void fill_cmd_array(char** cmd_array, char** argv) {
  //printf("I am filling cmd_array.\n");
  int cmd_array_ind = 0;
  int tmp_optind = optind;
  tmp_optind = tmp_optind + 2;
  while (argv[tmp_optind] != NULL && (argv[tmp_optind][0] != '-'
				      || argv[tmp_optind][1] != '-')) {
    cmd_array[cmd_array_ind++] = argv[tmp_optind++];
  }
  cmd_array[cmd_array_ind] = NULL;
}


int count_fds(int argc, char** argv) {
  int count = 0;
  int argv_ind = 1;
  char* opt;
  while (argv_ind < argc) {
    opt = argv[argv_ind];
    if (strcmp(opt, "--rdonly") == 0
	|| strcmp(opt, "--wronly") == 0
	|| strcmp(opt, "--rdwr") == 0 ) {
      count += 1;
    }
    else if (strcmp(opt, "--pipe") == 0) {
      count += 2;
    }
    argv_ind++;
  }
  return count;
}

int count_cmd_array_strings(int argc, char** argv) {
  int count = 0;
  int tmp_optind = optind;
  tmp_optind += 2; // tmp_optind indexes the beginning of the command.
  while (tmp_optind < argc && (argv[tmp_optind][0] != '-'
			       || argv[tmp_optind][1] != '-'))  {
    count++; tmp_optind++;
  }
  //printf("count_cmd_array_strings: %d\n", count);
  return count + 1; // including the null pointer at the end.
}

bool isDigit(char c) {
  return (c - '0' >= 0 && c - '0' < 10);
}

int count_commands(int argc, char** argv) {
  int count = 0;
  int argv_ind = 1;
  while (argv_ind < argc) {
    if (strcmp(argv[argv_ind++], "--command") == 0) {
      count++;
    }
  }
  return count;
}

void insert_to_cpids_to_cmds(pid_t cpid, char** cmd_array) {
  cpids_to_cmds[cpids_to_cmds_ind].cpid = cpid;
  cpids_to_cmds[cpids_to_cmds_ind].cmd_array = cmd_array;
  cpids_to_cmds_ind++;
}

void print_string_array(char** strings) {
  // strings must end with a NULL pointer
  int i = 0;
  while (strings[i] != NULL) {
    if (strings[i+1] == NULL) {
      // last string
      printf("%s", strings[i]);
    }
    else {
      printf("%s ", strings[i]);
    }
    i++;
  }
  printf("\n");
}

void get_cmd_array(pid_t cpid, char** string_array) {
  // return the cmd_aray that is mapped by cpid in cpids_to_cmds[]
  for (int i = 0; i < nCmds; i++) {
    if (cpids_to_cmds[i].cpid == cpid) {
      string_array = cpids_to_cmds[i].cmd_array;
      return;
    }
    /*else {

      }*/
  }
  string_array = NULL;
  fprintf(stderr, "ERROR: There is no command for cpid: %d\n", cpid);
}
 
int count_pipes(int argc, char** argv) {
  int count = 0;
  int argv_ind = 1;
  while (argv_ind < argc) {
    if (strcmp(argv[argv_ind], "--pipe") == 0) {
      count++;
    }
    argv_ind++; 
  }
}

bool wait_opt_exists(int argc, char** argv) {
  int argv_ind = 1;
  while (argv_ind < argc) {
    if (strcmp(argv[argv_ind++], "--wait") == 0)
      return true;
  }
  return false;
}

void timeval_difference(struct timeval* cpu_time,
		      const struct timeval* begin_time,
		      const struct timeval* end_time) {
  cpu_time->tv_sec = end_time->tv_sec - begin_time->tv_sec;
  cpu_time->tv_usec = end_time->tv_usec - begin_time->tv_usec;
}

void timeval_sum(struct timeval* cpu_time,
		 const struct timeval* t1,
		 const struct timeval* t2) {
  cpu_time->tv_sec = t1->tv_sec + t2->tv_sec;
  cpu_time->tv_usec = t1->tv_usec + t2->tv_usec;
}

#endif // SIMPSH_H_INCLUDED
