#!/bin/bash

function should_fail() {
  status=$?;

  if [ $status -lt 1 ]; then
      echo "FAILURE";
      exit 1;
  else
      echo;
  fi
}

function should_succeed() {
    status=$?;

    if [ $status -gt 0 ]; then
	echo "FAILURE";
	exit 1;
    else
	echo;
    fi
}

#./simpsh --rdonly input.txt --wronly output.txt --wronly error.txt --command 0 1 2 sort -u > /dev/null
#should_succeed "Program can write from input file to output file and write error to error file"

./simpsh --invalid_option 2>&1 | grep "invalid" > /dev/null
should_succeed "Program can detect an invalid option"

./simpsh --rdonly 2>&1 | grep "requires an argument" > /dev/null
should_succeed "Program can detect that an argument to an open command is missing"

./simpsh --rdonly --command 0 1 2 cat 2>&1 | grep "could not be opened" > /dev/null
should_succeed "Program can detect that a file could not be opened"

touch a
chmod 755 a
touch b
chmod 755 b
touch c
chmod 755 c
touch d
chmod 755 d
echo I am file a > a
echo I am file b > b
./simpsh \
--verbose \
--rdonly a \
--pipe \
--pipe \
--creat --trunc --wronly c \
--creat --append --wronly d \
--command 3 5 6 tr A-Z a-z \
--command 0 2 6 sort \
--command 1 4 6 cat b - \
	 --wait > /dev/null
should_succeed 

./simpsh --rdonly a_file_that_does_not_exist 2>&1 | grep "could not be opened" > /dev/null
should_succeed "can detect that a file could not be opened"

./simpsh --wronly a_file_that_does_not_exist 2>&1 | grep "could not be opened" > /dev/null
should_succeed "can detect that a file could not be opened"

./simpsh --rdwr a_file_that_does_not_exist 2>&1 | grep "could not be opened" > /dev/null
should_succeed "can detect that a file could not be opened"

./simpsh --creat --trunc --ignore 11
should_succeed "can silently ignore oflag options that do not open a file"

./simpsh \
    --rdonly a \
    --pipe \
    --pipe \
    --creat --trunc --wronly c \
    --creat --append --wronly d \
    --verbose \
    --command 3 5 6 tr A-Z a-z \
    --command 0 2 6 sort \
    --command 1 4 6 cat b - \
    --wait > /dev/null
rm a
rm b

#./simpsh --rdonly input.txt --wronly output.txt --wronly error.txt --command a b c cat 2>&1 | grep "command argument must be a number" > /dev/null
#should_succeed "Program can detect that input, output, or error is not a number"
