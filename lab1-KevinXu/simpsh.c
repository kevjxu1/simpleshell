#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/time.h>
#include "simpsh.h"

void sig_handler(int sig) {
  fprintf(stderr, "%d caught\n", sig);
  exit(sig);
}
// signal handler used for --catch option

int main(int argc, char** argv) {
  n_fds = count_fds(argc, argv);
  fd = (int*) malloc(n_fds * sizeof(int));

  nCmds = count_commands(argc, argv);
  cpids_to_cmds = (ChildToCommand*) malloc(nCmds * sizeof(ChildToCommand));

  nPipes = count_pipes(argc, argv);
  pipes = (Pipe*) malloc(nPipes * sizeof(Pipe));

  int c;
  int option_index = 0;
  int oflag_result = 0;
  bool verbose_on = false;
  bool profile_on = false;
  int i;
  
  while (1) {
    c = getopt_long(argc, argv, "", LONG_OPTS, &option_index);
    if (c == -1)
      break;
    
    int in, out, err;      
    int argv_ind;
    
    struct rusage r_usage;
    struct timeval u_begin_time, u_end_time, s_begin_time, s_end_time;
    struct timeval u_cpu_time, s_cpu_time, total_cpu_time;    
    
    switch (c) {
    case 0:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_APPEND;
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 1:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }

      oflag_result |= O_CLOEXEC;
      
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 2:     
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_CREAT;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 3:
      if (verbose_on) {	  
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_DIRECTORY;
      
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 4:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_DSYNC;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 5:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_EXCL;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 6:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      
      oflag_result |= O_NOFOLLOW;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 7:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_NONBLOCK;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 8:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_RSYNC;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 9:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_SYNC;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 10:
      if (verbose_on) {
	printf ("%s\n", argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_TRUNC;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 11: // rdonly
      if (verbose_on) {
	printf("%s %s\n", argv[optind - 2], argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_RDONLY;
      if (optarg) {
	fd[fd_ind++] = open(optarg, oflag_result);
	if (fd[fd_ind - 1] == -1) {
	  fprintf(stderr, "ERROR: %s could not be opened\n", optarg);
	}
	else {
	  // reset oflag_result
	  oflag_result = 0;
	}
      }	
      else if (optarg == NULL || optarg[0] == '-') {
	fprintf(stderr, "ERROR: the option %s requires an argument.\n",
		LONG_OPTS[option_index].name);
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
    case 12:
      if (verbose_on) {
	printf("%s %s\n", argv[optind - 2], argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_RDWR;
      if (optarg) {
	fd[fd_ind++] = open(optarg, oflag_result);

	if (fd[fd_ind - 1] == -1) {
	  fprintf(stderr, "ERROR: %s could not be opened\n", optarg);
	}
	else {
	  oflag_result = 0;
	}
      }
      else if (optarg == NULL || optarg[0] == '-'){ // if optarg == NULL
	fprintf(stderr, "ERROR: the option %s requires an argument.\n",
		LONG_OPTS[option_index].name);
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;	
    case 13:
      if (verbose_on) {
	printf("%s %s\n", argv[optind - 2], argv[optind - 1]);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      oflag_result |= O_WRONLY;
      if (optarg) {
	fd[fd_ind++] = open(optarg, oflag_result);
	if (fd[fd_ind - 1] == -1) {
	  fprintf(stderr, "ERROR: %s could not be opened\n", optarg);
	}
	else { // reset oflag_result after successful open
	  oflag_result = 0;
	}
      }
      else if (optarg == NULL || optarg[0] == '-') { // if optarg == NULL
	fprintf(stderr, "ERROR: the option %s requires an argument.\n",
		LONG_OPTS[option_index].name);
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;

    case 14: { // pipe
      if (verbose_on) {
	printf("--pipe\n");
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      if (pipe(pipe_fd) == -1) { // create pipes.
	fprintf(stderr, "ERROR: Failed to create a pipe\n");
      }
      else {
	fd[fd_ind++] = pipe_fd[0]; // add the read end of pipe
	fd[fd_ind++] = pipe_fd[1]; // add the write end of pipe

	pipes[pipes_ind].read_fd = pipe_fd[0];
	pipes[pipes_ind].write_fd = pipe_fd[1];
	pipes_ind++;
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
    } break;
      
    case 15: { // command" followed by all of its arguments
      if (verbose_on) {
	argv_ind = optind;
	if (optarg) {
	  printf("--command %s", optarg);
	}
	while(argv_ind < argc) {
	  if (argv[argv_ind][0] == '-' && argv[argv_ind][1] == '-') {
	    break;
	  }
	  printf(" %s", argv[argv_ind++]);
	}
	printf("\n");
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      
      cmd_array_size = count_cmd_array_strings(argc, argv);
      cmd_array = (char**) malloc(cmd_array_size * sizeof(char*));
      // Note: It is ok to reassign cmd_array without freeing memory
      // because the the memory originally pointed to by cmd_array
      // has new pointers to it, in cpids_to_cmds[]

      // check arguments to command
      if (optarg == NULL) {
	fprintf(stderr, "ERROR: command must have at least 4 arguments\n");
      }
      argv_ind = optind;
      while (argv_ind <= optind + 2 ) {
	if (argv[argv_ind] == NULL) {
	  fprintf(stderr, "ERROR: command must have at least 4 arguments\n");
	  break;
	} else if (argv_ind < optind + 2) {
	  i = 0;
	  while (argv[argv_ind][i] != '\0') {
	    if (!isDigit(argv[argv_ind][i++]))
	      fprintf(stderr, "ERROR: command argument must be a number\n");
	  }
	}
	argv_ind++;
      }
	 
      // get actual fds and store into in, out, err
      // command {in} {out} {err} {cmd_array}
      if (optarg != NULL) {
	in = fd[atoi(optarg)];
	if (argv[optind] != NULL) {
	  out = fd[atoi(argv[optind])];
	  if (argv[optind + 1] != NULL) {
	    err = fd[atoi(argv[optind + 1])];
	  }
	}
      }
      	
      //redirect(in, out, err);
      //dup2(in, 0);
      //dup2(out, 1);
      //dup2(err, 2);
      
      fill_cmd_array(cmd_array, argv);
      
      // update optind so that it indexes the next option
      optind = optind + 2;
      while (argv[optind] != NULL && (argv[optind][0] != '-'
				      || argv[optind][1] != '-')) {
	optind++;
      }
  
      if (cmd_array == NULL) {
	fprintf(stderr, "ERROR: Process is attempting to execute an empty command\n");
	continue;
      }

      pid_t pid;

      pid = fork();
      if (pid == -1) {
	fprintf(stderr, "ERROR: failed to fork child process\n");
      }
      else if (pid == 0) { // if child process
	redirect(in, out, err);	
	close_all_fds();	
	if (execvp(cmd_array[0], cmd_array) < 0) { // execute
	  fprintf(stderr, "ERROR: execution of %s failed\n",
		  cmd_array[0]);
	}
	exit(status);
      }
      else { // if parent process
	// keep track of which cmd_array belongs to which child process
	insert_to_cpids_to_cmds(pid, cmd_array);
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
    
    } break;
	
    case 16: { // wait
      if (verbose_on) {
	printf("--wait\n");
      }

      if (profile_on) {
	getrusage(RUSAGE_CHILDREN, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
	/*
	timeval u_begin_time_children, s_begin_time_children;
	getrusage(RUSAGE_CHILDREN, &r_usage);
	u_begin_time_children = r_usage.ru_utime;
	s_begin_time_children = r_usage.ru_stime;

	u_begin_time.tv_sec += u_begin_time_children.tv_sec;
	u_begin_time.tv_*/
      }
      
      // close all pipes
      for (i = 0; i < nPipes; i++) {
	close(pipes[i].read_fd);
	close(pipes[i].write_fd);
      }
      
      for (i = 0; i < nCmds; i++) { // for each child process
	int cpid = wait(&status);
	
	// update shell_exit_status to be the maximum of the processes'
	// exit statuses
	shell_exit_status =  shell_exit_status < WEXITSTATUS(status) ?
	  WEXITSTATUS(status) : shell_exit_status;
	
	if (cpid == -1) { // if error
	  fprintf(stderr, "ERROR: child process failed to exit successfully\n");
	} else {
	  // if no error
	  printf("%d ", WEXITSTATUS(status));

	  // get the cmd to print by searching for a match in
	  // cpids_to_cmds[]. 
	  char** cmd_to_print = NULL;       
	  for (int j = 0; j < nCmds; j++) {
	    if (cpid == cpids_to_cmds[j].cpid) {
	      cmd_to_print = cpids_to_cmds[j].cmd_array;
	      break;
	    }
	  }

	  // print the command
	  int ind = 0;
	  while (cmd_to_print[ind] != NULL) {
	    // for the entire array
	    if (cmd_to_print[ind + 1] == NULL) {
	      // if last word
	      printf("%s", cmd_to_print[ind]);
	    } else {
	      printf("%s ", cmd_to_print[ind]);
	    }
	    ind++;
	  }
	  printf("\n");
	}
      }

       if (profile_on) {
	getrusage(RUSAGE_CHILDREN, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }      
    } break;

    case 17: // --close N
      if (verbose_on) {
	printf("--close %s\n", optarg);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      // check whether option has an argument
      if (optarg == NULL || optarg[0] == '-') {
	fprintf(stderr, "ERROR: close requires an argument\n");
      }
      else {
	// check for valid argument
	int N = atoi(optarg);
	if (close(fd[N]) == -1) { // close file with logical fd N
	  // if error
	  fprintf(stderr, "ERROR: close failed\n");
	}
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;
	     
    case 18: // --verbose
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      verbose_on = true;

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;

    case 19: // --profile
      if (verbose_on) {
	printf("--profile\n");
      }
      profile_on = true;
      break;
    case 20: // --abort
      if (verbose_on) {
	printf("--abort\n");
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      // abort the program via seg fault
      if (raise(SIGSEGV) == -1) { // 
	fprintf(stderr, "ERROR: abort failed\n");
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      break;

    case 21: { // --catch N
      if (verbose_on) {
	printf("--catch %s\n", optarg);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      if (optarg == NULL || optarg[0] == '-') {
	fprintf(stderr, "ERROR: catch requires an argument\n");
      }
      else {
	int N = atoi(optarg);
	if (signal(N, sig_handler) == SIG_ERR) { // execute signal
	  // if error
	  fprintf(stderr, "ERROR: catch failed\n");
	} 
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
      
    } break;
      
    case 22: { // --ignore N
      if (verbose_on) {
	printf("--ignore %s\n", optarg);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      if (optarg == NULL || optarg[0] == '-') {
	fprintf(stderr, "ERROR: ignore requires an argument\n");
      }
      else {
	int N = atoi(optarg);
	signal(N, SIG_IGN); // ignore signal N
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
    } break;

    case 23: { // --default N
      if (verbose_on) {
	printf("--default %s\n", optarg);
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      if (optarg == NULL || optarg[0] == '-') {
	fprintf(stderr, "ERROR: default requires an argument\n");
      }
      else {
	int N = atoi(optarg);
	signal(N, SIG_DFL);
      }

      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
    } break;

    case 24: { // --pause
      if (verbose_on) {
	printf("--pause\n");
      }
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_begin_time = r_usage.ru_utime;
	s_begin_time = r_usage.ru_stime;
      }
      pause();
      
      if (profile_on) {
	getrusage(RUSAGE_SELF, &r_usage);
	u_end_time = r_usage.ru_utime;
	s_end_time = r_usage.ru_stime;

	timeval_difference(&u_cpu_time, &u_begin_time, &u_end_time);
	timeval_difference(&s_cpu_time, &s_begin_time, &s_end_time);
	timeval_sum(&total_cpu_time, &u_cpu_time, &s_cpu_time);

	printf("%s CPU time - user: %lds + %ldus - system: %lds + %ldus - ",
	       LONG_OPTS[option_index].name, u_cpu_time.tv_sec, u_cpu_time.tv_usec,
	       s_cpu_time.tv_sec, s_cpu_time.tv_usec);
	printf("total: %lds + %ldus\n", total_cpu_time.tv_sec,
	       total_cpu_time.tv_usec);
      }
    } break;
      
    case '?':
      fprintf(stderr, "ERROR: %s is an invalid option\n", argv[optind]);
      break;
      
    default:
      fprintf(stderr, "ERROR: getopt_long failed\n");

    }
  }
  
  free(pipes);
  free(fd);
  for (i = 0; i < nCmds; i++) {
    free(cpids_to_cmds[i].cmd_array);
  }
  exit(shell_exit_status);
}

